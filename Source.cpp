#include <iostream> // allows input and output to the console
#include "Interactive_Fiction.h" // allows access to the header file

using namespace std; // makes it so that you don't need std:: before each cout or cin
bool again;

void main() // runs on startup
{
	Fiction functions;
	Child retry;
	do
	{
		cout << "You are about to start a text based adventure it will ask you for inputs and change the story accordingly\n\n"; // outputs text
		system("pause"); // waits for user input
		system("CLS"); // clears th console
		cout << "Here marks the start of your adventure!!\n\n"; // outputs text
		cout << "Today is the day You finally turned 500!\n"; // outputs text
		cout << "After all your hard training you will finally be able to go out hunting\n"; // outputs text
		system("pause"); // waits for user input
		system("CLS"); // clears th console
		cout << "What will you hunt first?\n\n"; // outputs text
		cout << "1: Sheep\n"; // outputs text
		cout << "2: Frog\n"; // outputs text
		cout << "3: Cow\n"; // outputs text
		cout << "4: Unicorn\n"; // outputs text
		functions.FirstHunt(); // calls a function
		cout << "What do you think you are?\n"; // outputs text
		cout << "1: Lion\n"; // outputs text
		cout << "2: Bear\n"; // outputs text
		cout << "3: Dwarf\n"; // outputs text
		cout << "4: Dragon\n"; // outputs text
		functions.WhatAreYou(); // calls a function
		cout << "You're a dragon you get that now right?\n"; // outputs text
		cout << "Oh yeah I forgot to ask what is your name?\n"; // outputs text
		functions.WhoAreYou(); // calls a function
		functions.YourName(); // calls a function
		cout << "What color are you?\n"; // outputs text
		cout << "1: Black and Red\n"; // outputs text
		cout << "2: White and Gold\n"; // outputs text
		cout << "3: Blue and Purple\n"; // outputs text
		cout << "4: Green and Yellow\n"; // outputs text
		functions.WhatDoYouLookLikeColor(); // calls a function
		cout << "Now that that's out of the way what type of skin do you have?\n"; // outputs text
		cout << "1: Scaly\n"; // outputs text
		cout << "2: Feathery\n"; // outputs text
		cout << "3: Leathery\n"; // outputs text
		cout << "4: Spiky\n"; // outputs text
		functions.WhatDoYouLookLikeSkin(); // calls a function
		cout << "As you are about to leave to start your first hunt...\n"; // outputs text
		cout << "You hear the attack horn go off...\n"; // outputs text
		cout << "Looks like the hunt is cancelled... Sorry\n"; // outputs text
		cout << "Time to fight!\n"; // outputs text
		cout << "What is attacking?\n"; // outputs text
		cout << "1: Dwarves\n"; // outputs text
		cout << "2: Elves\n"; // outputs text
		cout << "3: Humans\n"; // outputs text
		cout << "4: Goblins\n"; // outputs text
		functions.Fight(); // calls a function
		functions.WhoWasSent(); // calls a function
		cout << "You rush to your kings defense!\n"; // outputs text
		cout << "How do you want to fight him?\n"; // outputs text
		cout << "1: Tail Swipe\n"; // outputs text
		cout << "2: Bite\n"; // outputs text
		cout << "3: Talk it out\n"; // outputs text
		cout << "4: Wind Gust\n"; // outputs text
		functions.WhatHappened(); // calls a function
		cout << "Do you want to play again?\n";
		cout << "1: Yes\n";
		cout << "2: no\n";
		retry.Again(again);
		system("pause"); // waits for user input
		system("CLS"); // clears th console
	}
	while (again && functions.alive);
	
}