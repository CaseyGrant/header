#include <iostream> // allows input and output to the console
#include <string> // allows the use of string functions
#include "Interactive_Fiction.h" // allows access to the header file

using namespace std; // makes it so that you don't need std:: before each cout or cin

int Fiction::GetNumbersFromUser() // a callable function
{
	string testNum; // a variable to store text
	getline(cin, testNum); // gets the whole line of text input and stores it in a variable
	try
	{
		num = stoi(testNum); // checks if the input is a number
	}
	catch (...)
	{
		cout << "Input must be a number!\n"; // outputs text
		GetNumbersFromUser(); // calls a function
	}
	return num; // returns with the number the user input
}

string Fiction::GetTextFromUser() // a callable function
{
	getline(cin, text); // gets the whole line of text input and stores it in a variable
	return text; // returns with the text the user input
}

void Fiction::FirstHunt() // a callable function
{
	GetNumbersFromUser(); // calls a function
	if (num == 1)
	{
		cout << "That's so cliche... Do you even know what you are?\n"; // outputs text
	}
	else if (num == 2)
	{
		cout << "Really you chose " << num << "?\n"; // outputs text
		cout << "Do you not know what you are?\n"; // outputs text
	}
	else if (num == 3)
	{
		cout << "Not a bad choice some nice hamburger!\n"; // outputs text
		cout << "Oh yeah do you know what you are?\n"; // outputs text
	}
	else if (num == 4)
	{
		cout << "That's a bit ambitious for your first hunt but I like your taste!\n"; // outputs text
		cout << "You don't remember what you are, do you?\n"; // outputs text
	}
	else
	{
		cout << "What were you thinking would happen?\n"; // outputs text
		cout << "Pick a number between 1-4...\n"; // outputs text
		FirstHunt(); // calls a function
	}
	system("pause");
	system("CLS");
}

void Fiction::WhatAreYou()
{
	GetNumbersFromUser();
	if (num == 1)
	{
		cout << "A lion really your 500!\n";
	}
	else if (num == 2)
	{
		cout << "A bear? No no no not even close!\n";
	}
	else if (num == 3)
	{
		cout << "Close but also so far from right!\n";
	}
	else if (num == 4)
	{
		cout << "You guessed it!\n";
	}
	else
	{
		cout << "Seriously this is an easy concept just pick one of the options?\n";
		cout << "Pick a number between 1-4...\n";
		WhatAreYou();
	}
	system("pause");
	system("CLS");
}

void Fiction::WhoAreYou()
{
	GetTextFromUser();
	name = text;
	cout << name << " that's really your name?\n";
	cout << "Okay if you say so...\n";
	system("pause");
	system("CLS");
}

void Fiction::YourName()
{
	cout << "Alright " << name << " What do you look like?\n";
}

void Fiction::WhatDoYouLookLikeColor()
{
	GetNumbersFromUser();
	if (num == 1)
	{
		cout << "Solid choice! You must be hard to see at night!\n";
	}
	else if (num == 2)
	{
		cout << "Shiny and bright! You must be hard to see in the clouds!\n";
	}
	else if (num == 3)
	{
		cout << "Dark but beautiful! You must be hard to see over the ocean!\n";
	}
	else if (num == 4)
	{
		cout << "An interesting mix! You must be hard to see near the land!\n";
	}
	else
	{
		cout << "Okay how many times are we gonna do this?\n";
		cout << "Pick a number between 1-4...\n";
		WhatDoYouLookLikeColor();
	}
	system("pause");
	system("CLS");
}

void Fiction::WhatDoYouLookLikeSkin()
{
	GetNumbersFromUser();
	if (num == 1)
	{
		cout << "Classic choice! Both pretty and effective!\n";
	}
	else if (num == 2)
	{
		cout << "Fair enough! Soft but maneuverable\n";
	}
	else if (num == 3)
	{
		cout << "What? that can't be comfortable how do you move?\n";
	}
	else if (num == 4)
	{
		cout << "Yikes! Offensive but uncomfortable...\n";
	}
	else
	{
		cout << "Are you kidding me you're this far in and still don't get it?\n";
		cout << "Pick a number between 1-4...\n";
		WhatDoYouLookLikeSkin();
	}
	system("pause");
	system("CLS");
}

void Fiction::Fight()
{
	GetNumbersFromUser();
	if (num == 1)
	{
		cout << "Dwarves are strong and well versed in combat!\n";
	}
	else if (num == 2)
	{
		cout << "Elves are agile and well versed in magic!\n";
	}
	else if (num == 3)
	{
		cout << "Humans are weak but smart\n";
	}
	else if (num == 4)
	{
		cout << "Goblins are small but there's so many of them!\n";
	}
	else
	{
		cout << "Okay this is getting ridiculous...\n";
		cout << "Pick a number between 1-4...\n";
		Fight();
	}
	system("pause");
	system("CLS");
}

void Fiction::WhoWasSent()
{
	if (num == 1)
	{
		cout << "The dwarves sent their champion to take down the dragon king\n";
	}
	else if (num == 2)
	{
		cout << "The elves sent their greatest magician to take down the dragon king\n";
	}
	else if (num == 3)
	{
		cout << "The humans sent their greatest gamer... Who happened to be from another realm.\n";
	}
	else if (num == 4)
	{
		cout << "The goblins send two squadrons of 500 goblins\n";
	}
}

void Fiction::WhatHappened()
{
	if (num == 1)
	{
		Dwarves(&alive);
	}
	else if (num == 2)
	{
		Elves(&alive);
	}
	else if (num == 3)
	{
		Humans(&alive);
	}
	else if (num == 4)
	{
		Goblins(&alive);
	}
}

void Fiction::Dwarves(bool* alive)
{
	GetNumbersFromUser();
	if (num == 1)
	{
		cout << "You go to swipe the dwarf with your tail\n";
		cout << "Unfortunately he had placed a trap and it cut your tail off...\n";
		cout << "Causing you to bleed to death...";
		*alive = false;
	}
	else if (num == 2)
	{
		cout << "You go to chomp down of the dwarf he sinks into the ground\n";
		cout << "This breaks all of your teeth...\n";
		cout << "This allows him to appear at your chest and stab you in the heart killing you \n";
		*alive = false;
	}
	else if (num == 3)
	{
		cout << "You ask the dwarf why he has come to kill the king.\n";
		cout << "He explains that the dragons are destroying their home to make nests.\n";
		cout << "You respond by informing him that the dragons were told that the area was uninhabited.\n";
		cout << "You promise that they will no longer make nests there and will help repair the area.\n";
		cout << "The attack was resolved without conflict and you went about your life.\n";
		*alive = true;
	}
	else if (num == 4)
	{
		cout << "You create a big gust of wind throwing the dwarf back into the wall\n";
		cout << "Killing him instantly... You started a war...\n";
		*alive = false;
	}
	else
	{
		cout << "Okay your doing this on purpose aren't you?\n";
		cout << "Pick a number between 1-4...\n";
		Dwarves(alive);
	}
	system("pause");
	system("CLS");
}

void Fiction::Elves(bool* alive)
{
	GetNumbersFromUser();
	if (num == 1)
	{
		cout << "The elf dodges under your tail and summons an icicle and thrusts it into your heart\n";
		cout << "You die... and the elf proceeded to kill the king\n";
		*alive = false;
	}
	else if (num == 2)
	{
		cout << "You go to bite the elf but he jumped on your head and stabbed you in both eyes and replaced your brain with acid...\n";
		cout << "In case it wasn't clear you died...\n";
		*alive = false;
	}
	else if (num == 3)
	{
		cout << "You ask the elf why he has come to kill the king.\n";
		cout << "He explains that the dragons are siphoning magic from the elves.\n";
		cout << "You respond by informing him that the dragons were not aware of this.\n";
		cout << "You promise that they would make sure to figure out the cause and help stop it.\n";
		cout << "The attack was resolved without conflict and you went about your life.\n";
		*alive = true;
	}
	else if (num == 4)
	{
		cout << "You go to hit the elf a blast of air but it seems to have no effect...\n";
		cout << "The elf then kills you with a quick summon of stalagmites and stalactites crushing your body\n";
		*alive = false;
	}
	else
	{
		cout << "Okay your doing this on purpose aren't you?\n";
		cout << "Pick a number between 1-4...\n";
		Elves(alive);
	}
	system("pause");
	system("CLS");
}

void Fiction::Humans(bool* alive)
{
	GetNumbersFromUser();
	if (num == 1)
	{
		cout << "You go to hit them with your tail but they dodge and end up turning you into a kabob... tasty...\n";
		cout << "You died...\n";
		*alive = false;
	}
	else if (num == 2)
	{
		cout << "You go to bite them but they jump back and stab through your head disconnecting your brain...\n";
		cout << "You live but its not much of a life...\n";
		*alive = false;
	}
	else if (num == 3)
	{
		cout << "You ask the human why he has come to kill the king.\n";
		cout << "He explains that the dragons are eating all of the sheep which making humans starve.\n";
		cout << "You respond by informing him that that was due to some fledgling dragons getting out.\n";
		cout << "You promise that they have been punished and it won't happen again.\n";
		cout << "The attack was resolved without conflict and you went about your life.\n";
		*alive = true;
	}
	else if (num == 4)
	{
		cout << "You go to hit them with a gust of wind but they bring out a shield and block the hit.\n";
		cout << "Then they charge you and stab you in the heart killing you\n";
		*alive = false;
	}
	else
	{
		cout << "Okay your doing this on purpose aren't you?\n";
		cout << "Pick a number between 1-4...\n";
		Humans(alive);
	}
	system("pause");
	system("CLS");
}

void Fiction::Goblins(bool* alive)
{
	GetNumbersFromUser();
	if (num == 1)
	{
		cout << "You hit one squadron with your tail killing half of them but the other half climbed on you\n";
		cout << "They shived you to death...\n";
		*alive = false;
	}
	else if (num == 2)
	{
		cout << "You managed to bite one of the squadrons killing some but the other squadron got on your head.\n";
		cout << "They deployed ladders off of you and repeatedly stabbed you in the heart.\n";
		*alive = false;
	}
	else if (num == 3)
	{
		cout << "You try to talk to the goblins but they just overtake you... They thought you were calling them names.\n";
		*alive = true;
	}
	else if (num == 4)
	{
		cout << "You hit the first squadron with a mighty gust killing most of them but the other squadron surrounded your feet.\n";
		cout << "They trip you and Stomp your body to death.\n";
		*alive = false;
	}
	else
	{
		cout << "Okay your doing this on purpose aren't you?\n";
		cout << "Pick a number between 1-4...\n";
		Goblins(alive);
	}
	system("pause");
	system("CLS");
}

void Child::Again(bool &again, bool* alive)
{
	GetNumbersFromUser();
	if (num == 1)
	{
		again = true;
		if (*alive == false)
		{
			cout << "You can't try again... You're dead remember...";
		}
	}
	else if (num == 2)
	{
		again = false;
	}
	else
	{
		cout << "There's only two choices it's not that hard\n";
		cout << "Pick a number between 1-2...\n";
		Again(again, alive);
	}
}