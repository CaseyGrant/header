#pragma once // allows other scripts to access this one
#include <string> // allows the use of string functions

using namespace std; // makes it so that you don't need std:: before each cout or cin

class Fiction
{
	public:
		int num; // a variable to store numbers
		bool alive;
		string GetTextFromUser(); // calls a function
		int GetNumbersFromUser(); // calls a function
		void FirstHunt(); // calls a function
		void WhatAreYou(); // calls a function
		void WhoAreYou(); // calls a function
		void YourName(); // calls a function
		void WhatDoYouLookLikeColor(); // calls a function
		void WhatDoYouLookLikeSkin(); // calls a function
		void Fight(); // calls a function
		void WhoWasSent(); // calls a function
		void WhatHappened(); // calls a function
		void Dwarves(bool *alive); // calls a function
		void Elves(bool* alive); // calls a function
		void Humans(bool* alive); // calls a function
		void Goblins(bool* alive); // calls a function
	private:
		string text; // a variable to store text
		string name; // a variable to store text
};

class Child : Fiction
{
	public:
		void Again(bool& again, bool* alive);
	private:
		
};